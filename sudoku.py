"""
Tool zur automatischen Berechnung von Sudokus
"""

import setfolder

# Initialisierung
inputvalues = {43: {2}, 57: {4}}
fixedvalues = {k: set(range(1, 10)) for k in range(11, 100) if not k % 10 == 0}

# Bekannte Werte werden in fixedvalues gesesetzt
fixedvalues.update({key: (fixedvalues[key] & inputvalues[key])
                    for key in inputvalues})

# Daraus abgeleitet werden die Werte der in Relation stehenden Felder
# eingegrenzt
for originkey in inputvalues:
    for folderkeys in setfolder.ranges(originkey):
        fixedvalues[folderkeys] = (fixedvalues[folderkeys] -
                                   fixedvalues[originkey])

i = len(inputvalues)
while i < 99:
    i += 1
