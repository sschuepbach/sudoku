"""
Modul: setfolder

Modul zum automatischen Setzen von Feldwerten in Abhängigkeit
zu einem bereits gesetzten Feldwert.
"""

def ranges(koord):
    """
    Diese Funktion berechnet die Koordinaten (xy) der Felder,
    deren mögliche Werte durch das Setzen eines Wertes für ein bestimmtes
    Feld eingeschränkt werden. D.h. also die Felder mit der gleichen
    x-Koordinate, mit der gleichen y-Koordinate oder im gleichen 9er-Feld.
    """
    koord = int(koord)
    rngsrtx = koord // 10 * 10 + 1
    koords = {n for n in range(rngsrtx, rngsrtx + 9)
              if not koord == n}
    rngsrty = koord % 10 + 10
    koords.update({n for n in range(rngsrty, rngsrty + 90, 10)
              if not koord == n})
    rngsrtfolderx = (((koord % 10) - 1) // 3) * 3 + 1
    rngsrtfoldery = (((koord // 10) - 1) // 3) * 30 + 10
    koords.update({n + o for n in range(rngsrtfoldery, rngsrtfoldery + 30, 10)
                   for o in range(rngsrtfolderx, rngsrtfolderx + 3)
                   if not koord == n + o})
    return koords

def updatevalues(value, koords, testvalues):
    for folderkey in koords:
        testvalues[folderkey] = (testvalues[folderkey] - {value})
        if testvalues[folderkey]:
            # Hier kommt Exception, wenn Feld keine Werte mehr enthält
            pass
    return testvalues
